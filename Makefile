.SUFFIXES:.c .o

CC=gcc

SRCS=crack.c
OBJS=$(SRCS:.c=.o)
EXEC=crack

start: $(OBJS)
	$(CC) -o $(EXEC) $(OBJS)

.c.o:
	$(CC) -Wall -o $@ -c $<

clean:
	rm -rf $(EXEC) $(OBJS)







