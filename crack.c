#include<stdio.h>
#include<math.h>
#include "sha256.c"
//check if a char[] matches any hash in an hash array
int trymatch(unsigned char guess[],unsigned char pwdhash[],int n)
{
	 SHA256_CTX ctx;
  unsigned char buf[32];
  int same=0;
	sha256_init(&ctx);
	sha256_update(&ctx, guess, strlen(guess));
	sha256_final(&ctx, buf);
   for(int k=0;k<n;k++)
   {
     same=1;
     for(int l=0;l<=31;l++)
   {
     if(buf[l]!=pwdhash[k*32+l])
     {
     same=0;
     break;
     }
   }
   if(same==1)
   {
   printf("%s %d\n",guess,k+1);
   break;
   }
   }
   return same;
}

int main(int argc, char * argv[])
{
  /*after doing some research, letters and numbers are two things people like best when thinking about a password.after googling and the analysis of common_passwords.txt,
  I finally generate a possibilty order of every letter appearing in common words, as shown below.the order combiles the possibility of appearance of a letter and its properties(e.g.,'e','a','o','i','u' are vewels, so I put them on the first 5 levels)*/
  
  unsigned char posgeneral[]={"eaoiutnhsrdlcmwyfgpbvkjxzq"};
  unsigned char posfirstletter[]={"taswihobmfcpdlngreyujkvqxz"};
if(argc==1)
{
  unsigned char pwdhash[999];
  FILE *fp= fopen("pwd4sha256","r");
  if(fp==NULL)
  {
	  printf("epmty file");
	  return 1;
  }
  int c;
  int index=0;
  while((c = fgetc(fp)) != EOF)
  {
    pwdhash[index++]=c;
  }
  unsigned char guess[7]="zhim";
  int findnum=0;
      for(int i=0;i<pow(10,4);i++)
      {
        guess[0]=(i/1000)%10+48;
        guess[1]=(i/100)%10+48;
        guess[2]=(i/10)%10+48;
        guess[3]=i%10+48;
        findnum+=trymatch(guess,pwdhash,10);
      }
	  for(int i=0;i<pow(26,4);i++)
	  {
		  guess[0]=posfirstletter[i%26];
		  guess[1]=posgeneral[(i/26)%26];
		  guess[2]=posgeneral[(int)(i/pow(26,2))%26];
		  guess[3]=posgeneral[(int)(i/pow(26,3))%26];
		  findnum+=trymatch(guess,pwdhash,10);  
	  }
	  for(int i=0;i<4;i++)
	  {
		  for(int j=0;j<10*pow(26,3);j++)
		  {
			  		  guess[i]=j%10+48;
					  guess[(i+1)%4]=posgeneral[(j/10)%26];
					  guess[(i+2)%4]=posgeneral[(j/10/26)%26];
					  guess[(i+3)%4]=posgeneral[(int)(j/10/pow(26,2))%26];
					  findnum+=trymatch(guess,pwdhash,10);
		  }
		  for(int j=0;j<26*pow(10,3);j++)
		  {
			  		  guess[i]=posgeneral[j%26];
					  guess[(i+1)%4]=(j/26)%10+48;
					  guess[(i+2)%4]=(j/26/10)%10+48;
					  guess[(i+3)%4]=(j/26/100)%10+48;
					  findnum+=trymatch(guess,pwdhash,10);
		  }
    }
	  for(int i=0;i<4;i++)
		{
			for(int j=0;j<pow(10,2)*pow(26,2);j++)
			{
				guess[i]=j%10+48;
				guess[(i+1)%4]=(j/10)%10+48;
				guess[(i+2)%4]=posgeneral[(j/10/26)%26];
				guess[(i+3)%4]=posgeneral[(j/10/26/26)%26];
				findnum+=trymatch(guess,pwdhash,10);
			}
		}
	 for(int i=0;i<2;i++)
	 {
		 for(int j=0;j<pow(10,2)*pow(26,2);j++)
			{
				guess[i]=j%10+48;
				guess[(i+2)%4]=(j/10)%10+48;
				guess[(i+1)%4]=posgeneral[(j/10/26)%26];
				guess[(i+3)%4]=posgeneral[(j/10/26/26)%26];
				findnum+=trymatch(guess,pwdhash,10);
			}
	 }

	fp= fopen("pwd6sha256","r");
  if(fp==NULL)
  {
	  printf("epmty file");
	  return 1;
  }
  while((c = fgetc(fp)) != EOF)
  {
    pwdhash[index++]=c;
  }
	findnum=0;

	  for(int i=0;i<pow(26,6);i++)
	  {
		  guess[0]=posfirstletter[i%26];
		  guess[1]=posgeneral[(i/26)%26];
		  guess[2]=posgeneral[(int)(i/pow(26,2))%26];
		  guess[3]=posgeneral[(int)(i/pow(26,3))%26];
		  guess[4]=posgeneral[(int)(i/pow(26,4))%26];
		  guess[5]=posgeneral[(int)(i/pow(26,5))%26];
		  findnum+=trymatch(guess,pwdhash,30);  
	  }
           for(int i=0;i<pow(10,6);i++)
      {
    guess[0]=(int)(i/pow(10,6))%10+48;
		guess[1]=(int)(i/pow(10,5))%10+48;
		guess[2]=(int)(i/pow(10,4))%10+48;
		guess[3]=(int)(i/pow(10,3))%10+48;
		guess[4]=(int)(i/pow(10,2))%10+48;
        guess[5]=(i/10)%10+48;
        findnum+=trymatch(guess,pwdhash,30);
      }
	  
	     for(int i=0;i<6;i++)
	  {
		  for(int j=0;j<10*pow(26,5);j++)
		  {
			  		  guess[i]=j%10+48;
					  guess[(i+1)%6]=posgeneral[(j/10)%26];
					  guess[(i+2)%6]=posgeneral[(j/10/26)%26];
					  guess[(i+3)%6]=posgeneral[(int)(j/10/pow(26,2))%26];
            guess[(i+4)%6]=posgeneral[(int)(j/10/pow(26,3))%26];
            guess[(i+5)%6]=posgeneral[(int)(j/10/pow(26,4))%26];
					  findnum+=trymatch(guess,pwdhash,30);
		  }
		  for(int j=0;j<26*pow(10,5);j++)
		  {
			  		  guess[i]=posgeneral[j%26];
					  guess[(i+1)%6]=(j/26)%10+48;
					  guess[(i+2)%6]=(j/26/10)%10+48;
					  guess[(i+3)%6]=(j/26/100)%10+48;
					  guess[(i+3)%6]=(j/26/1000)%10+48;
		        guess[(i+3)%6]=(j/26/10000)%10+48;
					  findnum+=trymatch(guess,pwdhash,30);
		  }		  
	  }
	  for(int i=0;i<6;i++)
		{
			for(int j=0;j<pow(10,2)*pow(26,4);j++)
			{
				guess[i]=j%10+48;
				guess[(i+1)%6]=(j/10)%10+48;
				guess[(i+2)%6]=posgeneral[(j/10/26)%26];
				guess[(i+3)%6]=posgeneral[(j/10/26/26)%26];
				guess[(i+4)%6]=posgeneral[(j/10/26/26/26)%26];
				guess[(i+5)%6]=posgeneral[(j/10/26/26/26/26)%26];
				findnum+=trymatch(guess,pwdhash,30);
			}
		}
	 for(int i=0;i<6;i++)
	 {
		 for(int j=0;j<pow(10,2)*pow(26,4);j++)
			{
				guess[i]=j%10+48;
				guess[(i+2)%4]=(j/10)%10+48;
				guess[(i+1)%4]=posgeneral[(j/10/26)%26];
				guess[(i+3)%4]=posgeneral[(j/10/26/26)%26];
				guess[(i+4)%4]=posgeneral[(j/10/26/26/26)%26];
				guess[(i+5)%4]=posgeneral[(j/10/26/26/26/26)%26];
				findnum+=trymatch(guess,pwdhash,30);
			}
	 }



   
  return 0;
 }
 else if(argc==2)
 {
     unsigned char guess[7];
     int guessnum;
     sscanf(argv[1],"%d",&guessnum);
   	  for(int i=0;i<pow(26,6);i++)
	  {
		  guess[0]=posfirstletter[i%26];
		  guess[1]=posgeneral[(i/26)%26];
		  guess[2]=posgeneral[(int)(i/pow(26,2))%26];
		  guess[3]=posgeneral[(int)(i/pow(26,3))%26];
		  guess[4]=posgeneral[(int)(i/pow(26,4))%26];
		  guess[5]=posgeneral[(int)(i/pow(26,5))%26];
       guessnum--;
       printf("%s\n",guess);
       if(guessnum==0)
         return 0;
	  }
           for(int i=0;i<pow(10,6);i++)
      {
    guess[0]=(int)(i/pow(10,6))%10+48;
		guess[1]=(int)(i/pow(10,5))%10+48;
		guess[2]=(int)(i/pow(10,4))%10+48;
		guess[3]=(int)(i/pow(10,3))%10+48;
		guess[4]=(int)(i/pow(10,2))%10+48;
        guess[5]=(i/10)%10+48;
       guessnum--;
       printf("%s\n",guess);
       if(guessnum==0)
         return 0;
      }
	  
	     for(int i=0;i<6;i++)
	  {
		  for(int j=0;j<10*pow(26,5);j++)
		  {
			  		  guess[i]=j%10+48;
					  guess[(i+1)%6]=posgeneral[(j/10)%26];
					  guess[(i+2)%6]=posgeneral[(j/10/26)%26];
					  guess[(i+3)%6]=posgeneral[(int)(j/10/pow(26,2))%26];
            guess[(i+4)%6]=posgeneral[(int)(j/10/pow(26,3))%26];
            guess[(i+5)%6]=posgeneral[(int)(j/10/pow(26,4))%26];
       guessnum--;
       printf("%s\n",guess);
       if(guessnum==0)
         return 0;
		  }
		  for(int j=0;j<26*pow(10,5);j++)
		  {
			  		  guess[i]=posgeneral[j%26];
					  guess[(i+1)%6]=(j/26)%10+48;
					  guess[(i+2)%6]=(j/26/10)%10+48;
					  guess[(i+3)%6]=(j/26/100)%10+48;
					  guess[(i+3)%6]=(j/26/1000)%10+48;
		        guess[(i+3)%6]=(j/26/10000)%10+48;
       guessnum--;
       printf("%s\n",guess);
       if(guessnum==0)
         return 0;
		  }		  
	  }
	  for(int i=0;i<6;i++)
		{
			for(int j=0;j<pow(10,2)*pow(26,4);j++)
			{
				guess[i]=j%10+48;
				guess[(i+1)%6]=(j/10)%10+48;
				guess[(i+2)%6]=posgeneral[(j/10/26)%26];
				guess[(i+3)%6]=posgeneral[(j/10/26/26)%26];
				guess[(i+4)%6]=posgeneral[(j/10/26/26/26)%26];
				guess[(i+5)%6]=posgeneral[(j/10/26/26/26/26)%26];
       guessnum--;
       printf("%s\n",guess);
       if(guessnum==0)
         return 0;
			}
		}
	 for(int i=0;i<6;i++)
	 {
		 for(int j=0;j<pow(10,2)*pow(26,4);j++)
			{
				guess[i]=j%10+48;
				guess[(i+2)%4]=(j/10)%10+48;
				guess[(i+1)%4]=posgeneral[(j/10/26)%26];
				guess[(i+3)%4]=posgeneral[(j/10/26/26)%26];
				guess[(i+4)%4]=posgeneral[(j/10/26/26/26)%26];
				guess[(i+5)%4]=posgeneral[(j/10/26/26/26/26)%26];
       guessnum--;
       printf("%s\n",guess);
       if(guessnum==0)
         return 0;
			}
	 }
  for(int i=0;i<95;i++)
    for(int i2=0;i2<95;i2++)
      for(int i3=0;i3<95;i3++)
        for(int i4=0;i4<95;i4++)
          for(int i5=0;i5<95;i5++)
            for(int i6=0;i6<95;i6++)
            {
              guess[0]=i+32;
              guess[1]=i2+32;
              guess[2]=i3+32;
              guess[3]=i4+32;
              guess[5]=i5+32;
              guess[6]=i6+32;
            }
 }
 else if(argc==3)
 {
 unsigned char pwdhash[50000];
  FILE *fp= fopen(argv[2],"r");
  if(fp==NULL)
  {
	  printf("epmty file");
	  return 1;
  }
  int c;
  int index=0;
  while((c = fgetc(fp)) != EOF)
  {
    pwdhash[index++]=c;
  }
  int hashnum=index/32;

   
  fp= fopen(argv[1],"r");
	  unsigned char test[10001];
 while(fgets(test,10000,fp)!=NULL)
  {
  if(test[strlen(test)-1]=='\n')
  test[strlen(test)-1]='\0';
   trymatch(test,pwdhash,hashnum);
  }

  return 0;
 }
 else
 printf("invalid command");
}